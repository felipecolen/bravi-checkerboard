from django.shortcuts import render

from checker_api.functions import generate_lines_list, generate_cols_list


def index(request, lines_size: int = 8, cols_last_letter: str = 'h'):
    lines_size_list = generate_lines_list(lines_size)
    columns_size_list = generate_cols_list(cols_last_letter)

    context = {
        'lines_size': lines_size_list,
        'columns_size': columns_size_list
    }
    return render(request, 'index.html', context=context)
