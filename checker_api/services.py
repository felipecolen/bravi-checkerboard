import logging

from checker_api.functions import generate_matrix_board, check_if_position_exists_in_matriz
from checker_api.models import ChessPiece, Log


logger = logging.getLogger(__name__)


def insert_log_in_db(coordinate: str, piece: ChessPiece, all_possible_locations: list):

    try:
        Log.objects.create(
            coordinate=coordinate,
            piece=piece,
            all_possible_locations=all_possible_locations
        )
    except Exception as e:
        logger.error(f'When trying to save the log contents to the database. Detail of error: {e.args}')


def simulate_chess_piece_locations(piece_id: int, coordinate: str,
                                   board_max_number: int = 8, board_max_letter: str = 'h'):
    possibilities_2d_turn = []
    valid_data, piece = validate_and_check_data(piece_id, coordinate)
    if valid_data:
        possibilities = get_possibilities_from_location_piece(coordinate, board_max_number, board_max_letter)
        possibilities_2d_turn = []
        for coordinate_in_possibility in possibilities:
            result = get_possibilities_from_location_piece(coordinate_in_possibility, board_max_number, board_max_letter)
            if result:
                possibilities_2d_turn.append((coordinate_in_possibility, result))

    insert_log_in_db(coordinate, piece, possibilities_2d_turn)
    return possibilities_2d_turn


def validate_and_check_data(piece_id: int, coordinate: str):
    """
        check if data content is valid
        :params piece_id and coordinate
        :return: bool
    """
    piece_is_knight, piece = validate_piece_and_check_if_knight(piece_id)
    coordinate_ok = validate_coordinate(coordinate)

    return piece_is_knight and coordinate_ok, piece


def validate_coordinate(coordinate: str):
    """
    check if coordinate contains one letter and one number, ex: A1
    :param coordinate:
    :return: bool
    """

    if len(coordinate) == 2:
        one_letter_ok = coordinate[0].isalpha()  # first char needs to be letter
        one_number_ok = coordinate[1].isdigit()  # second char needs to be number

        return one_letter_ok and one_number_ok

    return False  # if size different with two, coordinate invalid


def validate_piece_and_check_if_knight(piece_id: int):
    """
    check if piece exists and if piece is a knight
    :param piece_id:
    :return: boolean, ChessPiece
    """
    piece_exists = ChessPiece.objects.filter(id=piece_id)

    if piece_exists:
        piece = piece_exists.first()
        return piece.is_knight(), piece

    return False, None


def get_possibilities_from_location_piece(coordinate: str, board_max_number: int, board_max_letter: str):

    board = generate_matrix_board(board_max_number, board_max_letter)
    possible_postitions_knight = [
        [-1, -2], [1, -2],
        [-1, 2], [1, 2],
        [-2, 1], [-2, -1],
        [2, 1], [2, -1]
    ]
    possibilities = []

    location_line = int(coordinate[1])
    location_col = ord(coordinate[0])  # get number from letter

    for possibility in possible_postitions_knight:
        try:
            try_line = location_line + possibility[0]
            try_col = chr(location_col + possibility[1])  # get letter from number
            position = f'{try_col}{try_line}'
            if check_if_position_exists_in_matriz(matrix=board, position=position):
                if position not in possibilities:
                    possibilities.append(position)
        except:
            pass

    return possibilities


