
def generate_lines_list(lines_size: int = 8):
    lines_size_list = list(range(1, lines_size + 1))
    lines_size_list.sort(reverse=True)
    return lines_size_list


def generate_cols_list(cols_last_letter: str = 'h'):
    cols_last_letter = cols_last_letter.lower()
    letters_positions_list = range(ord('a'), ord(cols_last_letter) + 1)
    return [chr(letter_pos) for letter_pos in letters_positions_list]


def generate_matrix_board(board_max_number: int, board_max_letter: str):
    matrix = []
    for l in generate_lines_list(board_max_number):
        line = []
        for c in generate_cols_list(board_max_letter):
            line.append(f'{c}{l}')
        matrix.append(line)

    return matrix


def check_if_position_exists_in_matriz(matrix, position: str):
    for col, row in enumerate(matrix):
        if f'{position}' in row:
            return True
    return False
