from rest_framework import serializers

from checker_api.models import ChessPiece, Log


class ChessPieceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChessPiece
        fields = '__all__'


class LogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Log
        fields = '__all__'
