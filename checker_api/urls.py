"""checkerboard URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import include, path
from rest_framework import routers

from checker_api.views import index
from checker_api.views_api import ChessPieceViewSet, LogViewSet, get_all_locations_of_chess_piece

router = routers.DefaultRouter()
router.register(r'chess_pieces', ChessPieceViewSet)
router.register(r'logs', LogViewSet)


urlpatterns = [
    path('', index),
    path('<int:lines_size>/<str:cols_last_letter>/', index),

    path('api/', include(router.urls)),
    path('api/simulate/<int:piece_id>/<str:coordinate>/', get_all_locations_of_chess_piece),

    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
