from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response

from checker_api.models import ChessPiece, Log
from checker_api.serializers import ChessPieceSerializer, LogSerializer
from checker_api.services import simulate_chess_piece_locations


@api_view(['GET'])
def get_all_locations_of_chess_piece(request, piece_id: int, coordinate: str):
    if request.method == 'GET':
        return Response(simulate_chess_piece_locations(piece_id, coordinate))


class ChessPieceViewSet(viewsets.ModelViewSet):
    queryset = ChessPiece.objects.all()
    serializer_class = ChessPieceSerializer


class LogViewSet(viewsets.ModelViewSet):
    queryset = Log.objects.all()
    serializer_class = LogSerializer
