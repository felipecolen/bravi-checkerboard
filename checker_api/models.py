from django.contrib.auth.models import User
from django.db import models


class BaseModel(models.Model):
    """
    abstract model to allow inheritance of essential fields
    """

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    added_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL,
                                 editable=False, related_name='model_added_by_set')
    updated_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL,
                                   editable=False, related_name='model_updated_by_set')


class ChessPiece(BaseModel):
    """
    model to represent the chess piece object and its characteristics
    """
    piece_type_choices = (
        ('Knight', 'The Knight'),
        ('Bishop', 'The Bishop'),
        ('King', 'The King'),
        ('Pawn', 'The Pawn'),
        ('Queen', 'The Queen'),
        ('Rook', 'The Rook')
    )

    piece_type = models.CharField(max_length=6, choices=piece_type_choices)
    piece_name = models.CharField(max_length=10)
    dark_color = models.BooleanField('Dark color?', help_text='Only two color options available: dark or light')

    def __str__(self):
        return f'{self.id}: {self.get_piece_type_display()} {self.piece_name}'

    def is_knight(self):
        return self.piece_type == 'Knight'

    class Meta:
        ordering = ('-id', )
        unique_together = ('piece_type', 'piece_name', 'dark_color')  # to evitate duplicate peaces
        # ToDo add the validate method for all pieces, ex. permit just 1 queen dark, max 8 pawns, etc...


class Log(BaseModel):
    """
    model that defines the structure of a log of activities performed in the system
    """

    coordinate = models.CharField(max_length=2, help_text='In algebric notation, ex.: A3')
    piece = models.ForeignKey(ChessPiece, null=True, on_delete=models.SET_NULL)
    all_possible_locations = models.TextField(null=True, editable=False)

