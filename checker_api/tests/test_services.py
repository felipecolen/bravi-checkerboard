from unittest import TestCase

from checker_api.services import get_possibilities_from_location_piece


class ServicesTestClass(TestCase):

    def test_get_possibilities_from_location_piece(self):
        expected_results = {
            'a1': ['c2', 'b3'],
            'c6': ['a5', 'a7', 'e5', 'e7', 'd4', 'b4', 'd8', 'b8'],
            'd4': ['b3', 'b5', 'f3', 'f5', 'e2', 'c2', 'e6', 'c6'],
            'h8': ['f7', 'g6']
        }
        board_max_number = 8
        board_max_letter = 'h'

        for option in expected_results:
            resul = get_possibilities_from_location_piece(option, board_max_number, board_max_letter)
            expected_resul = expected_results.get(option)
            self.assertEquals(resul, expected_resul)

