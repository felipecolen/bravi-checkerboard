from unittest import TestCase

from checker_api.functions import generate_lines_list, generate_cols_list, generate_matrix_board, \
    check_if_position_exists_in_matriz


class FunctionsTestClass(TestCase):

    def test_generate_lines_list_return_a_list(self):
        for lines_size in range(1, 10):
            result = generate_lines_list(lines_size)
            self.assertEqual(list, type(result))
            self.assertNotEqual(str, type(result))

    def test_generate_lines_list_return_a_valid_list_content(self):
        lines_size = 4
        result = generate_lines_list(lines_size)
        expected_result = [4, 3, 2, 1]
        self.assertEquals(result, expected_result)

    def test_generate_lines_list_return_expected_list_size(self):
        for lines_size in range(1, 10):
            result = generate_lines_list(lines_size)
            self.assertFalse(len(result) != lines_size)
            self.assertTrue(len(result) == lines_size)

    def test_generate_cols_list_return_a_valid_list_content(self):
        cols_last_letter = 'd'
        result = generate_cols_list(cols_last_letter)
        expected_result = ['a', 'b', 'c', 'd']
        self.assertEquals(result, expected_result)

    def test_generate_matrix_board_return_a_valid_matrix(self):
        board_max_number = 2
        board_max_letter = 'b'
        result = generate_matrix_board(board_max_number, board_max_letter)
        expected_result = [
            ['a2', 'b2'],
            ['a1', 'b1'],
        ]
        self.assertEquals(result, expected_result)

    def test_check_if_position_exists_in_matriz(self):
        board_max_number = 2
        board_max_letter = 'b'
        matrix = generate_matrix_board(board_max_number, board_max_letter)

        for position in ('a1', 'b1', 'a2', 'b2'):
            result = check_if_position_exists_in_matriz(matrix, position)
            self.assertEquals(result, True)

        for position_out in ('a3', 'b6'):
            result = check_if_position_exists_in_matriz(matrix, position_out)
            self.assertEquals(result, False)


