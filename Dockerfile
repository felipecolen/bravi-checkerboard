FROM python:3.9.7-alpine3.14
MAINTAINER Felipe Colen <felipecolen@gmail.com>

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# add the dependencies to SO
RUN apk add --update --no-cache \
        bash tzdata build-base \
        jpeg-dev zlib-dev postgresql-dev && \
    cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime && \
    apk del tzdata && \
    rm -rf /root/.cache

WORKDIR /app

COPY requirements.txt .

RUN pip install --no-cache-dir --upgrade pip setuptools && \
    pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 8000
